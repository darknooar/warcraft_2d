# -*- coding: utf-8 -*-
'''
Created on 29 нояб.. 2017 �.

@author: NOOAR
''' 

import pygame
from Consts import *
from Player import *
from pygame.locals import *




class Main():
    def __init__(self, screen):
        self.screen = screen
        self.player = Player('Slende Men')
        self.background = pygame.image.load('main_bg.jpg')
        self.running = True
        self.main_loop()
    
    def handle_events(self):
        for event in pygame.event.get():
            if event.type == QUIT:
                self.running = False
            
            # Run Charter
            elif event.type == KEYDOWN:
                
                # Right
                if event.key == K_l:
                    self.player.moving = [1,0,0,0]
                
                # Down
                if event.key == K_k:
                    self.player.moving = [0,1,0,0]
                    
                # Left
                if event.key == K_j:
                    self.player.moving = [0,0,1,0]
                
                # Up    
                if event.key == K_i:
                    self.player.moving = [0,0,0,1]
                    
                if event.key == K_z:
                    if self.player.state == ALIVE:
                        self.player.state = DEAD
                    else:
                        self.player.state = ALIVE    
                
                if event.key == K_SPACE:
                    if self.player.state == ALIVE:
                        self.player.state = SHOOT
                    else:
                        self.player.state = ALIVE  
                
            #Stop run after key-up
            elif event.type == KEYUP:
                if event.key == K_i:
                    self.player.moving[UP] = 0
                
                if event.key == K_k:
                    self.player.moving[DOWN] = 0
                
                if event.key == K_l:
                    self.player.moving[RIGHT] = 0
                    
                if event.key == K_j:
                    self.player.moving[LEFT] = 0
                    
           
                    
            
        
    def render(self):
        self.screen.blit(self.background, (0,0))
        self.player.render(screen)
        pygame.display.flip()
    
    def main_loop(self):
        while self.running == True:
            if self.player.state != DEAD:
                self.player.move()
            self.render()
            self.handle_events()
            
            
    

pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

game = Main(screen)










