'''
Created on 29 нояб. 2017 �.

@author: NOOAR
'''


import pygame
from Consts import *



class Player():
    
    def __init__(self, name):    
        self.state      = ALIVE
        self.direction  = DOWN
        self.x = START_X
        self.y = START_Y
        self.hp = MAX_HP
        self.mp = MAX_MP
        self.name = name
        self.images_pack = ['data/archerr.png', 'data/archerd.png', 'data/archerl.png', 'data/archeru.png']
        self.images = []
        for image in self.images_pack:
            temp = pygame.image.load(image).convert_alpha()
            i = []
            i.append(temp.subsurface(0,0,64,64))     # ALIVE 
            i.append(temp.subsurface(64,0,64,64))    # SHOOT
            i.append(temp.subsurface(128,0,64,64))   # DEAD
            self.images.append(i)
        
        self.moving = [0,0,0,0]
        
    def move(self):
        if self.moving[RIGHT] == 1:
            self.direction = RIGHT
            self.x += PLAYER_SPEED
        if self.moving[DOWN] == 1:
            self.direction = DOWN
            self.y += PLAYER_SPEED
        if self.moving[LEFT] == 1:
            self.direction = LEFT
            self.x -= PLAYER_SPEED
        if self.moving[UP] == 1:
            self.direction = UP
            self.y -= PLAYER_SPEED
            
            
        # Ограничения на выход top left    
        if self.x <= 0:
            self.x = 0
        if self.y <= 0:
            self.y = 0

        # Ограничения на выход bottom right
        if self.x >= SCREEN_WIDTH - 60:
            self.x = SCREEN_WIDTH - 60
        if self.y >= SCREEN_HEIGHT - 64:
            self.y = SCREEN_HEIGHT - 64
    
    def render(self, screen):
        screen.blit(self.images[self.direction][self.state], (self.x, self.y))
    
    def render_ui(self, screen):
        pass
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        